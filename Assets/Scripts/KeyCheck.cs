﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCheck : MonoBehaviour
{
    private Transform[] doors;
    private bool isOpen = false;
    [SerializeField]
    private GameObject message;

    private void Start()
    {
        doors = transform.GetComponentsInChildren<Transform>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player") || isOpen)
            return;

        if(GameManager.Instance.inventory.hasKey == true)
        {
            doors[1].transform.position += new Vector3(0, 0, 1);
            doors[2].transform.position += new Vector3(0, 0, -1);
            isOpen = true;
            GameManager.Instance.PlaySuccessSound();
        }
        else
        {
            message.SetActive(true);
            GameManager.Instance.PlayErrorSound();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            message.SetActive(false);
        }
    }
}
