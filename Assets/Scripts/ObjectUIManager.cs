﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// It would probably be more efficient to just render a blacked out sprite of the object but I found this way o do it from Unity with the 3D model
public class ObjectUIManager : MonoBehaviour
{
    private GameObject[] objects;
    private int blackLayer;
    private int lightLayer;

    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        objects = GameObject.FindGameObjectsWithTag("ObjectProjection");
        blackLayer = LayerMask.NameToLayer("ObjectPanel");
        lightLayer = LayerMask.NameToLayer("ObjectPanelLight");
    }

    public void BlackoutObjects()
    {
        foreach (GameObject i in objects)
        {
            i.layer = blackLayer;
            foreach (Transform child in i.transform)
                child.gameObject.layer = blackLayer;
        }
    }

    public void ActivateLight(int n)
    {
        objects[n].layer = lightLayer;
        foreach (Transform child in objects[n].transform)
            child.gameObject.layer = lightLayer;
    }
}
