﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class EndingCheck : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.Instance.Director.stopped += goToEndScreen;
            GameManager.Instance.Director.Play();
            FindObjectOfType<LightRainbowing>().StartCycle();
            GameManager.Instance.wonGame = true;
        }
    }

    private void goToEndScreen(PlayableDirector pd) => GameManager.Instance.GoToEndScreen();
}
