﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    private Transform player;

    private void Start()
    {
        player = GameManager.Instance.Player.transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.position.x, player.position.y + 20f, player.position.z);
        var euler = Camera.main.transform.rotation.eulerAngles;
        var rot = Quaternion.Euler(90, 0, -euler.y);
        transform.rotation = rot;
    }
}
