﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireDamage : MonoBehaviour, IDamageable
{
    public void OnDamage()
    {
        GameManager.Instance.KillPlayer();
    }
}
