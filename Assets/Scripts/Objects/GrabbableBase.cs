﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GrabbableBase : MonoBehaviour, IGrabbable
{
    [SerializeField]
    private Color outlineColor;
    private GameObject mapMarker;

    private void Start()
    {
        mapMarker = Instantiate(Resources.Load<GameObject>("MapIcon"), transform.parent);
        mapMarker.transform.localScale = mapMarker.transform.localScale / 2;
        mapMarker.GetComponent<SpriteRenderer>().color = outlineColor;

        var outline = gameObject.AddComponent<Outline>();
        outline.OutlineMode = Outline.Mode.OutlineVisible;
        outline.OutlineColor = outlineColor;
        outline.OutlineWidth = 3f;
    }

    public virtual void OnGrab()
    {
        GameManager.Instance.PlayGrabSound();
    }
}
