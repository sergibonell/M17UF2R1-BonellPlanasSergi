﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComplementLogic : GrabbableBase
{
    [SerializeField]
    private GameObject complement;
    [SerializeField]
    private ObjectTypes objectType;

    public override void OnGrab()
    {
        base.OnGrab();
        complement.SetActive(true);
        GameManager.Instance.GrabObject(objectType);
    }
}
