﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyLogic : GrabbableBase
{
    public override void OnGrab()
    {
        base.OnGrab();
        GameManager.Instance.GrabObject(ObjectTypes.Key);
    }
}
