﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    private ObjectUIManager projectionsScript;

    public GameObject Player;
    public PlayableDirector Director;
    public bool isDead = false;
    public bool wonGame = false;

    public InventorySO inventory;
    [SerializeField]
    private AudioClip grabObjectSound;
    [SerializeField]
    private AudioClip errorSound;
    [SerializeField]
    private AudioClip successSound;
    private AudioSource audioSource;
    

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Game Manager is NULL!");
            }

            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
        audioSource = GetComponent<AudioSource>();
        inventory.clearAll();
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        projectionsScript = GameObject.Find("3DView").GetComponent<ObjectUIManager>();
        projectionsScript.BlackoutObjects();
    }

    public void KillPlayer()
    {
        isDead = true;
        Player.GetComponent<CharacterMove>().killPlayer();
    }

    public void GoToEndScreen()
    {
        Debug.Log("FINISHED");
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("EndScreen");
    }

    public void PlayGrabSound()
    {
        audioSource.PlayOneShot(grabObjectSound);
    }

    public void PlayErrorSound()
    {
        audioSource.PlayOneShot(errorSound);
    }

    public void PlaySuccessSound()
    {
        audioSource.PlayOneShot(successSound);
    }

    public void GrabObject(ObjectTypes type)
    {
        switch (type)
        {
            case ObjectTypes.Hat:
                inventory.hasHat = true;
                projectionsScript.ActivateLight(0);
                break;
            case ObjectTypes.Weapon:
                inventory.hasWeapon = true;
                projectionsScript.ActivateLight(1);
                break;
            case ObjectTypes.Tool:
                inventory.hasTool = true;
                projectionsScript.ActivateLight(2);
                break;
            case ObjectTypes.Key:
                inventory.hasKey = true;
                projectionsScript.ActivateLight(3);
                break;
            default:
                break;
        }
    }
}

public enum ObjectTypes
{
    Hat,
    Tool,
    Weapon,
    Key
}
