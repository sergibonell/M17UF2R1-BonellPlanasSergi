﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterMove : MonoBehaviour
{
    private Animator animator;
    private PlayerInputActions inputActions;
    private CharacterController characterController;
    private Transform cameraTransform;

    // input values
    [SerializeField]
    private Vector2 rawInput;
    [SerializeField]
    private Vector2 smoothedInput = Vector2.zero;
    [SerializeField]
    Vector3 rotatedInput;
    [SerializeField]
    private Vector3 walkInput;
    [SerializeField]
    private Vector3 runInput;

    private Vector2 inputSmoothVelocity;
    [SerializeField]
    private float inputSmoothSpeed;

    [SerializeField]
    private bool isMovementPressed;
    [SerializeField]
    private bool isRunPressed;
    [SerializeField]
    private bool isCrouchedPressed;
    [SerializeField]
    private bool isJumpPressed;
    [SerializeField]
    private bool isAimPressed;
    [SerializeField]
    private bool isGrabPressed;
    [SerializeField]
    private bool interactionBlocked;
    [SerializeField]
    private bool isShootPressed;


    [SerializeField]
    private float gravity;
    [SerializeField]
    private float jumpHeight;
    [SerializeField]
    private float walkSpeed;
    [SerializeField]
    private float runSpeed;
    [SerializeField]
    private float rotationSpeed;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController>();
        inputActions = new PlayerInputActions();
        cameraTransform = Camera.main.transform;
        Cursor.lockState = CursorLockMode.Locked;

        inputActions.Player.Move.started += onMovementInput;
        inputActions.Player.Move.performed += onMovementInput;
        inputActions.Player.Move.canceled += onMovementInput;

        inputActions.Player.Run.started += onRun;
        inputActions.Player.Run.canceled += onRun;

        inputActions.Player.Crouch.started += onCrouch;

        inputActions.Player.Jump.started += onJump;

        inputActions.Player.Aim.started += onAim;
        inputActions.Player.Aim.canceled += onAim;

        inputActions.Player.Interact.started += onInteract;

        inputActions.Player.Dance.started += onDance;

        inputActions.Player.Shoot.started += onShoot;
    }

    private void OnEnable()
    {
        inputActions.Player.Enable();
    }

    private void OnDisable()
    {
        inputActions.Player.Disable();
    }

    private void Update()
    {
        handleAnimation();

        if (!interactionBlocked)
        {
            handleGravity();
            smoothMovement();
            handleRotation();
            handleJump();
            handleInteraction();
            handleMove();
            handleShooting();
        }
    }

    private void onMovementInput(InputAction.CallbackContext context)
    {
        rawInput = context.ReadValue<Vector2>();
        isMovementPressed = rawInput.magnitude != 0;
    }
    private void onRun(InputAction.CallbackContext context) => isRunPressed = context.ReadValueAsButton();
    private void onCrouch(InputAction.CallbackContext context) => isCrouchedPressed = !isCrouchedPressed;
    private void onJump(InputAction.CallbackContext context) => isJumpPressed = characterController.isGrounded;
    private void onAim(InputAction.CallbackContext context) => isAimPressed = context.ReadValueAsButton();
    private void onInteract(InputAction.CallbackContext context) => isGrabPressed = context.ReadValueAsButton();
    private void onDance(InputAction.CallbackContext context) => interactionBlocked = context.ReadValueAsButton();
    private void onShoot(InputAction.CallbackContext context) => isShootPressed = context.ReadValueAsButton();

    private void handleMove()
    {
        if (isRunPressed && rawInput.y >= 0)
        {
            characterController.Move(runInput * Time.deltaTime);
            isCrouchedPressed = false;
        }
        else
            characterController.Move(walkInput * Time.deltaTime);
    }
    
    private void handleRotation()
    {
        if (isMovementPressed || isAimPressed)
        {
            Quaternion targetRotation = Quaternion.Euler(0, cameraTransform.eulerAngles.y, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }
    }
    
    private void handleAnimation()
    {
        bool isCrouched = animator.GetBool("Crouched");
        bool isJumping = animator.GetBool("Jumping");
        bool isAiming = animator.GetBool("Aiming");
        bool isGrabbing = animator.GetBool("Grabbing");
        bool isShooting = animator.GetBool("Shooting");

        Vector3 localVelocity = characterController.velocity.x * transform.right * -1 + characterController.velocity.z * transform.forward;
        animator.SetFloat("VelocityX", localVelocity.x);
        animator.SetFloat("VelocityZ", localVelocity.z);

        if (!isCrouched && isCrouchedPressed)
            animator.SetBool("Crouched", true);
        else if (isCrouched && !isCrouchedPressed)
            animator.SetBool("Crouched", false);

        if (!isJumping && isJumpPressed)
            animator.SetBool("Jumping", true);
        else if(isJumping && !isJumpPressed && characterController.isGrounded)
            animator.SetBool("Jumping", false);

        if (!isAiming && isAimPressed && !interactionBlocked)
            animator.SetBool("Aiming", true);
        else if (isAiming && !isAimPressed)
            animator.SetBool("Aiming", false);

        if (!isGrabbing && isGrabPressed && characterController.isGrounded && !interactionBlocked)
            animator.SetBool("Grabbing", true);
        else if (isGrabbing && !isGrabPressed)
            animator.SetBool("Grabbing", false);

        if (interactionBlocked && !GameManager.Instance.isDead && GameManager.Instance.Director.state != UnityEngine.Playables.PlayState.Playing)
            GameManager.Instance.Director.Play();

        if (!isShooting && isShootPressed)
            animator.SetBool("Shooting", true);
        else if (isShooting && !isShootPressed)
            animator.SetBool("Shooting", false);
    }

    private void handleGravity()
    {
        if (characterController.isGrounded)
        {
            walkInput.y = -1f;
            runInput.y = -1f;
        }
        else
        {
            walkInput.y += gravity * Time.deltaTime;
            runInput.y += gravity * Time.deltaTime;
        }
    }

    private void handleJump()
    {
        if(isJumpPressed && !isCrouchedPressed)
        {
            walkInput.y += Mathf.Sqrt(jumpHeight * -3.0f * gravity);
            runInput.y += Mathf.Sqrt(jumpHeight * -3.0f * gravity);
        }
        isJumpPressed = false;
    }

    private void handleInteraction()
    {
        if (isGrabPressed)
        {
            isGrabPressed = false;
        }
    }

    private void handleShooting()
    {
        if (isShootPressed || !isAimPressed)
        {
            isShootPressed = false;
        }
    }

    public void freeInteraction()
    {
        interactionBlocked = false;
    }

    public void blockInteraction()
    {
        interactionBlocked = true;
    }

    public void killPlayer()
    {
        animator.SetTrigger("Dead");
        interactionBlocked = true;
    }

    private void smoothMovement()
    {
        smoothedInput = Vector2.SmoothDamp(smoothedInput, rawInput, ref inputSmoothVelocity, inputSmoothSpeed);

        if (smoothedInput.magnitude < 0.01f)
            smoothedInput = Vector2.zero;

        rotatedInput = smoothedInput.x * cameraTransform.right + smoothedInput.y * cameraTransform.forward;
        Vector2 dir = new Vector2(rotatedInput.x, rotatedInput.z);
        if (isMovementPressed)
            dir.Normalize();
        rotatedInput = new Vector3(dir.x, 0, dir.y);
        walkInput = rotatedInput * walkSpeed + walkInput.y * Vector3.up;
        runInput = rotatedInput * runSpeed + runInput.y * Vector3.up;
    }
}
