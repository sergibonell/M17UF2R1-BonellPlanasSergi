﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightRainbowing : MonoBehaviour
{
    private Light barnLight;
    [SerializeField]
    private Color[] colors;
    private int currentColor = 0;

    private void Start()
    {
        barnLight = GetComponent<Light>();
    }

    public void StartCycle()
    {
        StartCoroutine("colorCycle");
    }

    IEnumerator colorCycle()
    {
        while (true)
        {
            if (currentColor < colors.Length - 1)
                currentColor++;
            else
                currentColor = 0;

            barnLight.color = colors[currentColor];
            yield return new WaitForSeconds(0.5f);
        }
    }

}
