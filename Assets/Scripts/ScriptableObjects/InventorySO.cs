﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "ScriptableObjects/Inventory", order = 1)]
public class InventorySO : ScriptableObject
{
    public bool hasHat = false;
    public bool hasTool = false;
    public bool hasWeapon = false;
    public bool hasKey = false;

    public void clearAll()
    {
        hasHat = false;
        hasTool = false;
        hasWeapon = false;
        hasKey = false;
    }
}
