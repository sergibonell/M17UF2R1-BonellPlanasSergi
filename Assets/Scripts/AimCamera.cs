﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AimCamera : MonoBehaviour
{
    private PlayerInputActions inputActions;
    private CinemachineVirtualCamera virtualCamera;
    [SerializeField]
    private GameObject aimReticle;
    [SerializeField]
    private GameObject hipReticle;
    [SerializeField]
    private int priorityBoostAmount;

    private void Awake()
    {
        virtualCamera = GetComponent<CinemachineVirtualCamera>();
        inputActions = new PlayerInputActions();

        inputActions.Player.Aim.started += _ => startAim();
        inputActions.Player.Aim.canceled += _ => cancelAim();
    }

    private void OnEnable()
    {
        inputActions.Player.Enable();
    }

    private void OnDisable()
    {
        inputActions.Player.Disable();
    }

    private void startAim()
    {
        virtualCamera.Priority += priorityBoostAmount;
        aimReticle.SetActive(true);
        hipReticle.SetActive(false);
    }

    private void cancelAim()
    {
        virtualCamera.Priority -= priorityBoostAmount;
        aimReticle.SetActive(false);
        hipReticle.SetActive(true);

    }
}
