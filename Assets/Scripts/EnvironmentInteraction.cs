﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class EnvironmentInteraction : MonoBehaviour
{
    [SerializeField]
    GameObject currentObject = null;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<IGrabbable>() != null)
            currentObject = other.gameObject;
        if (other.GetComponent<IDamageable>() != null)
            other.GetComponent<IDamageable>().OnDamage();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<IGrabbable>() != null)
            currentObject = null;
    }

    public void GrabObject()
    {
        if(currentObject != null)
        {
            Destroy(currentObject.transform.parent.gameObject);
            currentObject.GetComponent<IGrabbable>().OnGrab();
            currentObject = null;
        }
    }
}
