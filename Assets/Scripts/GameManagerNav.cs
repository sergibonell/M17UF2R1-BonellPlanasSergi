﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class GameManagerNav : MonoBehaviour
{
    private static GameManagerNav _instance;
    public GameObject Player;
    public Transform[] waypoints;
    public bool isSeen = false;

    public static GameManagerNav Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Game Manager is NULL!");
            }

            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {

    }
}

