﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class NavManager : MonoBehaviour
{
    private PlayerInputActions inputActions;
    private NavMeshAgent agent;
    private Animator animator;

    [SerializeField]
    private States state;
    [SerializeField]
    private bool followingPlayer = true;

    [SerializeField]
    private Transform point;
    [SerializeField]
    private Transform player;
    private Vector3 currentDestination;

    private void Awake()
    {
        inputActions = new PlayerInputActions();
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        inputActions.Player.AgentDestination.started += onAgentDest;
    }

    private void OnEnable()
    {
        inputActions.Player.Enable();
    }

    private void OnDisable()
    {
        inputActions.Player.Disable();
    }

    private void onAgentDest(InputAction.CallbackContext context) => followingPlayer = !followingPlayer;

    // Start is called before the first frame update
    void Start()
    {
        currentDestination = player.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (followingPlayer)
            currentDestination = player.position;
        else
            currentDestination = point.position;

         agent.SetDestination(currentDestination);

        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            if (followingPlayer)
                state = States.Attacking;
            else
                state = States.Idle;
        }
        else
            state = States.Walking;

        handleAnimation();
    }

    void handleAnimation()
    {
        bool isAttacking = animator.GetBool("Attacking");

        if (!isAttacking && state == States.Attacking)
            animator.SetBool("Attacking", true);
        else if(isAttacking && state != States.Attacking)
            animator.SetBool("Attacking", false);

        Vector3 localVelocity = agent.velocity.x * transform.right * -1 + agent.velocity.z * transform.forward;
        animator.SetFloat("VelocityX", localVelocity.x);
        animator.SetFloat("VelocityZ", localVelocity.z);
    }
}

enum States
{
    Idle,
    Walking,
    Attacking
}
