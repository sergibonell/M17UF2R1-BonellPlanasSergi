using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheKiwiCoder;

public class GetNewWaypoint : ActionNode
{

    protected override void OnStart() {
    }

    protected override void OnStop() {
    }

    protected override State OnUpdate() {
        if (blackboard.waypointIndex < GameManagerNav.Instance.waypoints.Length - 1)
            blackboard.waypointIndex++;
        else
            blackboard.waypointIndex = 0;

        blackboard.moveToPosition = GameManagerNav.Instance.waypoints[blackboard.waypointIndex].position;
        return State.Success;
    }
}
