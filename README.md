# M17UF2R1-BonellPlanasSergi

## R1.1
### Controls
- WASD or Arrow Keys: Movement
- Left Shift: Run (can't run backwards)
- C: Toggle crouch
- Right Click: Aim gun
- Left Click (while aiming): Shoot gun
- Space: Jump
- T: Dance (controls and UI are disabled)
- E: Grab

## R1.2
### Grabbable items
- You can grab multiple objects in scene "R1.2.2 Interiors". 
- These objects are the "chainsaw", "pitchfork", "shotgun", "axe" and "can".

## R1.3
- Grabbable items changed to "bucket", "pitchfork", "shears" and "squash"
- All points from the Classroom task should be implemented and working (base and extras)

## R1.4
- First and second scenes with state management using switch
- Third scene with the Behaviour Tree implemented
- Behaviour Tree contamplates patrolling, chasing, attacking and going back to patrol after set time of not seeing the player
